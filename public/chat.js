// Make connection
// var socket = io.connect('http://localhost:4000');
let url = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
console.log(url);
var socket = io.connect(`${url}`);

// Query DOM
var message = document.getElementById('message'),
    handle = document.getElementById('handle'),
    btn = document.getElementById('send'),
    output = document.getElementById('output'),
    feedback = document.getElementById('feedback');

//get saved handle
handle.value = localStorage.getItem('handle');

// Emit events
btn.addEventListener('click', function () {
    //save handle
    localStorage.setItem('handle', handle.value);

    //send message
    if (handle.value) {
        socket.emit('chat', {
            message: message.value,
            handle: handle.value
        });
        message.value = "";
    }
});

message.addEventListener('keypress', function () {
    socket.emit('typing', handle.value);
})

// Listen for events
socket.on('chat', function (data) {
    feedback.innerHTML = '';
    output.innerHTML += '<p><strong>' + data.handle + ': </strong>' + data.message + '</p>';

    //auto-scroll
    let chat = document.getElementById('chat-window');
    chat.scrollTop = chat.scrollHeight;
});

socket.on('typing', function (data) {
    feedback.innerHTML = '<p><em>' + data + ' is typing a message...</em></p>';
});
